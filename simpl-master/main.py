#!/usr/bin/env python

# -*- coding: utf-8 -*-
# warning - here be dragons!

import sys
from PySide import QtCore, QtGui
import cv2
import signal

from fheadparser import parse_fhead, Argument, ArgUIWidget
import glob
import inspect
import string
import os
import imp
from numpy import ndarray

# RW SUPPORT
#
# File type     OCV-R       OCV-W
# bmp           +           +
# jpg           +           +
# jpeg          +           +
# png           +           +
# ppm           +           +
#
# #


class ImageHistoryEntry:
    def __init__(self, n, i, s):
        self.name = n
        self.image = i
        self.saved = s


class FilterEntry:
    def __init__(self, fo, fs, fm):
        self.function_obj = fo
        self.slug = fs

        # unwrapping fhead
        self.filter_name = fm.name
        self.filter_desc = fm.desc
        self.function_name = fm.funcname
        self.filter_args = fm.args


class FilterParams:
    def __init__(self, slug, args, argv, idx):
        self.slug = slug
        self.arg_specs = args
        self.arg_vals = argv
        self.idx = idx


class QSFilterDialog(QtGui.QDialog):
    def __init__(self, name, args, parent = None):
        """
        QSFilterDialog constructor. Takes a filter name for the dialog title
        and an array of arguments depending on which a filter dialog is 
        constructed.

        :param name: name of the filter
        :type name: str
        :param args: array of Arguments
        :type args: list of Argument
        :param parent: dialog parent object
        :type parent: QtGui.QWidget
        """

        super(QSFilterDialog, self).__init__(parent)

        self.setWindowTitle(name)
        self.setModal(True)

        self.args = args
        self.widgets = []
        self.lyt = QtGui.QFormLayout()

        # initial window size suggested by the layout after adding widgets
        # (or sizeHint()) is usually the prettiest one - this will tell the
        # layout not to let the dialog to be resized
        self.lyt.setSizeConstraint(QtGui.QLayout.SizeConstraint.SetFixedSize)

        i = 0
        for a in self.args:
            if a.ui_widget == ArgUIWidget.QSpinBox:
                w = QtGui.QSpinBox(self)
            elif a.ui_widget == ArgUIWidget.QDoubleSpinBox:
                w = QtGui.QDoubleSpinBox(self)
            elif a.ui_widget == ArgUIWidget.QComboBox:
                w = QtGui.QComboBox(self)
                for v in a.string_values:
                    w.addItem(v)
            elif a.ui_widget == ArgUIWidget.QCheckBox:
                w = QtGui.QCheckBox(self)

            w.setObjectName(a.key_name)

            if a.ui_widget == ArgUIWidget.QSpinBox or a.ui_widget == ArgUIWidget.QDoubleSpinBox:
                if a.limit_lower is not None:
                    w.setMinimum(a.limit_lower)
                else:
                    minv = -2**31
                    w.setMinimum(minv)

                if a.limit_upper is not None:
                    w.setMaximum(a.limit_upper)
                else:
                    maxv = 2**31 - 1
                    w.setMaximum(maxv)

            self.widgets.append(w)
            self.lyt.addRow(string.join([a.gui_name, ':'], ''), self.widgets[i])
            i += 1

        # TODO[RS]: creating widgets without specifying parent is a potential memory leak (https://stackoverflow.com/questions/15892723/getting-value-from-qspinbox-created-in-a-qformlayout)
        self.btns = QtGui.QDialogButtonBox(
            QtGui.QDialogButtonBox.Ok | QtGui.QDialogButtonBox.Cancel
        )
        self.btns.accepted.connect(self.accept)
        self.btns.rejected.connect(self.reject)
        self.lyt.addRow(self.btns)
        self.setLayout(self.lyt)

    def run(self):
        r = self.exec_()
        if r == QtGui.QDialog.Accepted:
            d = {}

            i = 0
            for w in self.widgets:
                arg_uiw = self.args[i].ui_widget
                if arg_uiw == ArgUIWidget.QSpinBox or arg_uiw == ArgUIWidget.QDoubleSpinBox:
                    v = w.value()
                elif arg_uiw == ArgUIWidget.QComboBox:
                    v = w.currentText()
                elif arg_uiw == ArgUIWidget.QCheckBox:
                    v = w.isChecked()

                n = w.objectName()
                d[n] = v

                i += 1

            return d
        else:
            return None


class QSMain(QtGui.QMainWindow):
    def __init__(self, path, no_err_dlgs):
        super(QSMain, self).__init__()

        self.app_cwd = sys.path[0]

        # TODO[RS]: migrate this to a parent class of some sort?
        self.cur_img = None
        self.cur_img_path = ""

        self.zoom_levels = [8, 4, 2, 1, 0.667, 0.5, 0.333, 0.25, 0.182, 0.125]
        self.zoom_idx_default = 3
        self.zoom_idx = self.zoom_idx_default

        self.filters = []
        self.image_history = []
        self.image_history_idx = 0

        self.last_filter = None
        self.last_filter_params_reusable = False

        self.setup()

        if path is not None:
            self.open_image(path)

        self.no_error_dlgs = no_err_dlgs


    def setup(self):
        self.setObjectName("MainWindow")
        self.resize(405, 300)  # initial window size
        # TODO[FR]: icon!
        self.setWindowIcon(QtGui.QIcon.fromTheme("applications-graphics"))
        self.setWindowTitle("SIMPL")


        # menu_bar definition
        self.menu_bar = QtGui.QMenuBar(self)
        self.menu_bar.setObjectName("menu_bar")

        # new menu named "File"
        self.menu_file = self.menu_bar.addMenu("&File")

        # open action - used in menu_bar and tool_bar
        self.file_open_action = QtGui.QAction(QtGui.QIcon.fromTheme("document-open"), "&Open...", self)
        self.file_open_action.setStatusTip("Open an image file")
        self.file_open_action.setShortcut(QtGui.QKeySequence.Open)
        self.file_open_action.triggered.connect(self.file_open)

        # now add it to the menu_file menu
        self.menu_file.addAction(self.file_open_action)

        # save action - used in menu_bar and tool_bar
        self.file_save_action = QtGui.QAction(QtGui.QIcon.fromTheme("document-save"), "&Save", self)
        self.file_save_action.setStatusTip("Save the current image over the existing one")
        self.file_save_action.setShortcut(QtGui.QKeySequence.Save)
        self.file_save_action.triggered.connect(self.file_save)

        # add action
        self.menu_file.addAction(self.file_save_action)

        # save as action - used in menu_bar and tool_bar
        self.file_saveas_action = QtGui.QAction(QtGui.QIcon.fromTheme("document-save-as"), "&Save As...", self)
        self.file_saveas_action.setStatusTip("Save the current image under the specified name")
        self.file_saveas_action.setShortcut(QtGui.QKeySequence.SaveAs)
        self.file_saveas_action.triggered.connect(self.file_saveas)

        # add action
        self.menu_file.addAction(self.file_saveas_action)

        # a separator before the quit action
        self.menu_file.addSeparator()

        # close action
        self.file_quit_action = QtGui.QAction(QtGui.QIcon.fromTheme("application-exit"), "&Quit", self)
        self.file_quit_action.setStatusTip("Quit SIMPL")
        self.file_quit_action.setShortcut(QtGui.QKeySequence.Quit)
        self.file_quit_action.triggered.connect(self.close)

        # add action
        self.menu_file.addAction(self.file_quit_action)

        # "Edit" menu
        # TODO[FR]: advanced undo history: ...
        # with a:
        # * dockable dialog
        # * thumbnail of each image in image_history
        #
        # or a simpler initial approach:
        # * scrollable menu with only 6 (or so) items visible
        # * list of all the image history steps with a bullet point next to the
        #   current one
        # * QActionGroup with QActionGroup.exclusive property set to True
        self.menu_edit = self.menu_bar.addMenu("&Edit")

        # undo/redo actions - usage: menu_bar, tool_bar
        self.edit_undo_action = QtGui.QAction(QtGui.QIcon.fromTheme("edit-undo"), "&Undo", self)
        self.edit_undo_action.setStatusTip("Undo last action")
        self.edit_undo_action.setShortcut(QtGui.QKeySequence.Undo)
        self.edit_undo_action.triggered.connect(self.edit_undo)

        self.menu_edit.addAction(self.edit_undo_action)

        self.edit_redo_action = QtGui.QAction(QtGui.QIcon.fromTheme("edit-redo"), "&Redo", self)
        self.edit_redo_action.setStatusTip("Redo last action")
        self.edit_redo_action.setShortcut(QtGui.QKeySequence.Redo)
        self.edit_redo_action.triggered.connect(self.edit_redo)

        self.menu_edit.addAction(self.edit_redo_action)

        # "View" menu
        self.menu_view = self.menu_bar.addMenu("&View")

        # zoom out/in actions - usage: menu_bar, tool_bar
        self.view_zoomout_action = QtGui.QAction(QtGui.QIcon.fromTheme("zoom-out"), "&Zoom out", self)
        self.view_zoomout_action.setStatusTip("Zoom out")
        self.view_zoomout_action.setShortcut(QtGui.QKeySequence.ZoomOut)
        self.view_zoomout_action.triggered.connect(self.view_zoomout)

        self.menu_view.addAction(self.view_zoomout_action)

        self.view_zoomin_action = QtGui.QAction(QtGui.QIcon.fromTheme("zoom-in"), "&Zoom in", self)
        self.view_zoomin_action.setStatusTip("Zoom in")
        self.view_zoomin_action.setShortcut(QtGui.QKeySequence.ZoomIn)
        self.view_zoomin_action.triggered.connect(self.view_zoomin)

        self.menu_view.addAction(self.view_zoomin_action)

        # filter menu
        # new menu named "Filters"
        self.menu_filters = self.menu_bar.addMenu("F&ilters")

        # TODO: find a hotkey for these guys?
        self.filters_reloadfilters_action = QtGui.QAction(QtGui.QIcon.fromTheme("view-refresh"), "&Reload filters", self)
        self.filters_reloadfilters_action.setStatusTip("Reload available filters")
        self.filters_reloadfilters_action.triggered.connect(self.reload_filters_menu)

        self.menu_filters.addAction(self.filters_reloadfilters_action)

        self.filters_reapplylastfilter_action = QtGui.QAction(QtGui.QIcon.fromTheme("emblem-system"), "&Reapply last filter", self)
        self.filters_reapplylastfilter_action.setStatusTip("Reapply last filter")
        self.filters_reapplylastfilter_action.setEnabled(False)
        self.filters_reapplylastfilter_action.triggered.connect(self.reapply_last_filter)

        self.menu_filters.addAction(self.filters_reapplylastfilter_action)

        self.menu_filters.addSeparator()

        # an array of filter actions to group them all!
        self.filters_actions = []

        self.populate_filters_menu()



        # menu_bar's done, let's add it to the QMainWindow
        self.setMenuBar(self.menu_bar)


        # tool_bar definition
        self.tool_bar = QtGui.QToolBar(self)
        self.tool_bar.setObjectName("tool_bar")
        self.tool_bar.setMovable(False)

        # zoom dropdown list widget definition
        self.tool_bar_zoomdropdown = QtGui.QComboBox(self)
        self.tool_bar_zoomdropdown.activated.connect(self.view_zoomlvlchanged)
        for zl in self.zoom_levels:
            self.tool_bar_zoomdropdown.addItem(str(zl * 100) + "%")
        self.tool_bar_zoomdropdown.setCurrentIndex(self.zoom_idx_default)

        # toolbar reload and reapply last filter action
        self.tool_bar_reloadreapplyfilter = QtGui.QAction(QtGui.QIcon.fromTheme("view-refresh"), "Reload and reapply last filter", self)
        self.tool_bar_reloadreapplyfilter.setStatusTip("Reload filters and reapply last filter if exists")
        self.tool_bar_reloadreapplyfilter.triggered.connect(self.reload_reapply_last_filter)
        self.tool_bar_reloadreapplyfilter.setEnabled(False)

        # now add the earlier defined actions
        self.tool_bar.addAction(self.file_open_action)
        self.tool_bar.addSeparator()
        self.tool_bar.addAction(self.file_save_action)
        self.tool_bar.addAction(self.file_saveas_action)
        self.tool_bar.addSeparator()
        self.tool_bar.addAction(self.view_zoomout_action)
        self.tool_bar.addWidget(self.tool_bar_zoomdropdown)
        self.tool_bar.addAction(self.view_zoomin_action)
        self.tool_bar.addSeparator()
        self.tool_bar.addAction(self.edit_undo_action)
        self.tool_bar.addAction(self.edit_redo_action)
        self.tool_bar.addSeparator()
        self.tool_bar.addAction(self.tool_bar_reloadreapplyfilter)

        self.update_uielem_states()


        # tool_bar's done, let's add it to the QMainWindow
        self.addToolBar(self.tool_bar)

        # imgpreview defs
        self.imgpreview_scene = QtGui.QGraphicsScene()
        self.imgpreview_scene_checkerboardbrush = QtGui.QBrush(QtGui.QPixmap(self.abs_path("res/gimp_checkerboard.png")))
        self.imgpreview_scene.setBackgroundBrush(QtCore.Qt.darkGray)
        self.imgpreview = QtGui.QGraphicsView(self.imgpreview_scene, self)
        self.imgpreview.setDragMode(QtGui.QGraphicsView.ScrollHandDrag)
        self.imgpreview_curimg = QtGui.QGraphicsPixmapItem()
        self.imgpreview_scene.addItem(self.imgpreview_curimg)

        # imgpreview's done, let's add it to the QMainWindow
        self.setCentralWidget(self.imgpreview)


        # status_bar definition
        self.status_bar = QtGui.QStatusBar(self)
        self.status_bar.setObjectName("status_bar")

        # main status label
        self.current_status_msg = QtGui.QLabel(self)
        self.current_status_msg.setText("No image opened yet. ")
        self.status_bar.addWidget(self.current_status_msg)

        # status_bar created, now add it to the QMainWindow
        self.setStatusBar(self.status_bar)

        # TODO[RS]: show() should be in main()? (see https://github.com/PySide/Examples/blob/master/examples/mainwindows/menus.py)
        self.show()

    def show_err_dlg(self, txt, dtxt = None):
        if self.no_error_dlgs:
            if dtxt is None:
                print(txt)
            else:
                print(txt + "\nThe error reported was:\n\t" + dtxt)

        else:
            err_dlg = QtGui.QMessageBox(self)
            err_dlg.setWindowTitle("Error")
            err_dlg.setIconPixmap(
                QtGui.QIcon.fromTheme("dialog-error").pixmap(32, 32)
            )
            err_dlg.setText(txt)

            if dtxt is not None:
                err_dlg.setDetailedText(dtxt)

            err_dlg.exec_()

    def show_statusbar_msg(self, msg):
        self.status_bar.showMessage(msg, 2000)

    def file_open(self):
        if self.handle_file_leave():
            cur_file_name = QtGui.QFileDialog.getOpenFileName(
                parent=self,
                caption="Open Image",
                dir=self.get_current_directory(),
                filter="Image Files (*.bmp *.jpg *.jpeg *.png *.ppm)"
            )

            if cur_file_name[0] != "":
                self.open_image(cur_file_name[0])

    def open_image(self, path):
        self.cur_img = cv2.imread(path, cv2.IMREAD_UNCHANGED)

        if self.cur_img is not None:
            # if image is successfully opened:
            #  * reset img history,
            #  * add the original one as first (zeroth in computer
            #    lingo) in row,
            #  * set the current image modified flag to False (it's a
            #    freshly opened image, no modifications yet),
            #  * update the current image path
            #  * set current zoom index to the default one,
            #  * flush the image widget and undo menu items
            self.image_history = []
            self.image_history.append(
                ImageHistoryEntry("Original image", self.cur_img, True)
            )
            self.image_history_idx = 0
            self.cur_img_path = path

            self.zoom_idx = self.zoom_idx_default
            self.update_window()
        else:
            msg = string.join(
                ["An error occurred while opening '", path, "'."], ''
            )
            self.show_err_dlg(msg)
            self.show_statusbar_msg(msg)

    def is_cur_img_saved(self):
        if len(self.image_history) > 0:
            return self.image_history[self.image_history_idx].saved
        else:
            # nothing is opened
            return True

    def update_imgpreview(self):
        # TODO[RS]: QImage out of memory when zooming large images
        # TODO[RS]: Blender PNGs look awful
        curimg = self.cur_img

        # if we happen to deal with a grayscale image, cur_img.shape should be
        # a tuple of 2
        if (len(curimg.shape)) < 3:
            curimg_qimgfmt = QtGui.QImage.Format_Indexed8
            curimgdata_channum = 1
        else:
            curimgdata_channum = curimg.shape[2]

            if curimgdata_channum == 4:
                curimg_qimgfmt = QtGui.QImage.Format_ARGB32
            elif curimgdata_channum == 3:
                curimg_qimgfmt = QtGui.QImage.Format_RGB888

        curimgdata_width = curimg.shape[1]
        curimgdata_height = curimg.shape[0]
        # fetching bytes per line (bpl = stride = bpp * width) to properly draw
        # RGB888; grayscale and 4ch ARGB32 can live without it. taking
        # bpp * width into consideration, curimg_strides could also be
        # calculated as:
        # curimg_strides = curimgdata_channum * curimgdata_width
        curimg_strides = curimg.strides[0]

        curimg_qimage = QtGui.QImage(
            curimg.data, curimgdata_width, curimgdata_height, curimg_strides,
            curimg_qimgfmt
        )

        # if channel number is 3, we've got a BGR image
        # -- QImage expects an RGB image, so we have to swap B and R channels
        if curimgdata_channum == 3:
            curimg_qimage = curimg_qimage.rgbSwapped()

        curimg_qpixmap = QtGui.QPixmap(curimg_qimage)
        cur_zoomfac = self.zoom_levels[self.zoom_idx]
        curimg_size = curimg_qpixmap.size() * cur_zoomfac
        curimg_qpixmap = curimg_qpixmap.scaled(curimg_size)

        # if image has alpha, draw checkerboard. else Qt.darkGray
        if curimgdata_channum == 4:
            self.imgpreview_scene.setBackgroundBrush(
                self.imgpreview_scene_checkerboardbrush
            )
        else:
            self.imgpreview_scene.setBackgroundBrush(QtCore.Qt.darkGray)

        self.imgpreview_curimg.setPixmap(curimg_qpixmap)

        self.imgpreview_scene.setSceneRect(
            QtCore.QRectF(0, 0,
                          curimgdata_width * cur_zoomfac,
                          curimgdata_height * cur_zoomfac)
        )

        self.current_status_msg.setText("Image: " + str(curimgdata_width)
                                        + " x " + str(curimgdata_height)
                                        + " pixels, " + str(curimgdata_channum)
                                        + " channel(s)")

        self.update_uielem_states()
        self.tool_bar_zoomdropdown.setCurrentIndex(self.zoom_idx)

    def handle_file_leave(self):
        if not self.is_cur_img_saved():
            # setting dialog parent to a proper QWidget (in this case self or
            # QSMain) hides the dialog from the taskbar, as it ought to be for
            # modal dialogs
            warning_save_dlg = QtGui.QMessageBox(self)
            warning_save_dlg.setWindowTitle("Close Image")
            warning_save_dlg.setIconPixmap(
                QtGui.QIcon.fromTheme("document-save").pixmap(32, 32)
            )
            warning_save_dlg.setText(
                "The image has been modified. Do you want to save changes?"
            )
            warning_save_dlg.setStandardButtons(QtGui.QMessageBox.Save |
                                                QtGui.QMessageBox.Discard |
                                                QtGui.QMessageBox.Cancel)
            state = warning_save_dlg.exec_()

            if state == QtGui.QMessageBox.Discard:
                return True
            elif state == QtGui.QMessageBox.Save:
                if self.pick_save_image():
                    return True
                else:
                    return False
            elif state == QtGui.QMessageBox.Cancel:
                return False
            else:
                # this should never be reached, but is nevertheless written in
                # PySide documentation
                warning_save_dlg.exec_()

        else:
            return True

    def get_current_directory(self):
        if self.cur_img_path != "":
            return self.cur_img_path.rsplit('/', 1)[0]
        else:
            return self.cur_img_path

    def file_save(self):
        self.save_file(self.cur_img_path)

    def file_saveas(self):
        self.pick_save_image()

    def pick_save_image(self):
        # TODO[FR]: subclass QFileDialog to facilitate image-type-specific ...
        # ... file saving parameters, like, e.g. CV_IMWRITE_PNG_COMPRESSION
        # it could also contain the file name cleanup procedure below

        file_to_save = QtGui.QFileDialog.getSaveFileName(
            parent=self,
            caption="Save Image",
            dir=self.get_current_directory(),
            filter="JPEG image [.jpeg] (*.jpeg);;"
                   "JPEG image [.jpg] (*.jpg);;"
                   "PNG image [.png] (*.png);;"
                   "PPM image [.ppm] (*.ppm);;"
                   "Windows BMP image [.bmp] (*.bmp)"
        )

        path = file_to_save[0]

        # if user exits file dialog without picking a file name, abort
        if path == "":
            return False

        # OpenCV can't save files if extension (suffix) is not specified, so
        # let's first fetch the selected extension and prepare it for suffixing
        suffix = file_to_save[1].split("(")
        suffix = suffix[len(suffix) - 1]
        suffix = suffix.strip(")").strip("*")
        # TODO[RS]: find a better solution for the above written stripping ...
        # ... procedure (e.g. translate + maketrans) + bench on 100k calls vs.
        # current solution
        # -> https://stackoverflow.com/questions/3900054/python-strip-multiple-characters

        # TODO[FR]: All (Supported) Image Files filter which then decodes ...
        # ... suffix to a proper one, else default (.png)?
        # -> research default suffixes in QFileDialog.

        # let's check if the user has typed in the proper extension. if not,
        # we'll strip it and add the proper one
        path_split = os.path.split(path)
        filename = path_split[1]

        filename_split = filename.rsplit('.', 1)
        filename_base = filename_split[0]

        if len(filename_split) == 1:
            filename_ext = ''
        else:
            filename_ext = filename_split[len(filename_split) - 1]

        # construct a save new path if the typed extension is different from
        # the one picked in the dropdown
        # lower() included in case user specifies the extension in uppercase or
        # any other letter case
        if string.lower(filename_ext) != suffix.strip('.'):
            path = string.join([path_split[0], '/', filename_base, suffix], '')

        if self.save_file(path):
            self.cur_img_path = path
            self.update_title()
            return True
        else:
            return False

    def save_file(self, path):
        if cv2.imwrite(path, self.cur_img):
            self.show_statusbar_msg(string.join(
                ["File '", path, "' successfully saved."], '')
            )
            if not self.image_history[self.image_history_idx].saved:
                for ihe in self.image_history:
                    ihe.saved = False
                self.image_history[self.image_history_idx].saved = True
                self.update_title()
            return True
        else:
            msg = string.join(
                ["An error has occurred while saving file '", path, "'."], ''
            )
            self.show_err_dlg(msg)
            self.show_statusbar_msg(msg)
            return False

    def reload_filters_menu(self):
        # TODO[FR]: RELOAD FILTERS AND REAPPLY LAST ONE (WIP!)
        #  filters menu items:
        #   reload -> always clickable
        #   reapply -> only if there is a previously executed filter
        #    (IHI not zero) (plus filter exists after reload - handled below)
        #     (nullify lastFilterParams so that uus can check lFP!=NULL and
        #      IHI!=0)
        #   -> simply lFP!=NULL? (seems to work for now)
        #
        #   fused toolbar button -> reload and reapply only if lFP not NULL
        #
        #   ** RELOAD ** (probably revising populate_filters_menu, or just wrap
        #    it?)
        #   1. flush FilterEntries
        #   2. call populate_filters_menu()
        #   3. compare lFP / lFA
        #   4. check Reload QAction state
        #
        #
        #   consider that after R&R the filter might no longer exist,
        #   or it might simply be located at another location -> store py
        #    filename, seek filter entry associated with the stored py name
        #    -> could handle the sort by name TO-DO as well (not really, slugs
        #       might not be similar to actual names AT ALL)
        #
        #    -> store filter slug (filters-FILTERNAME), seek slug on reload and
        #       compare lastFilterArgs types with new FilterMeta args [DON'T
        #       PERFORM COMPARISON IF lFA/lFP NULL!]
        #    -> if okay, enable QAction Reapply and set text to
        #       Reapply FILTERNAME (e.g. Reapply Gaussian Blur Awesominator -
        #       check GIMP to see how literals are handled there)
        #    -> else disable QAction and set text to Reapply last filter
        #    -> filter list as QActionGroup? (research actual usage)
        #
        #   might need to break update_uielem_states into multiple functions as
        #    irrelevant things are being refreshed
        #    (another TO-DO)
        #   -> reload might need to disable reapply if the previous filter
        #      doesn't exist
        #   -> ... or just bool? previous_filter_exists defaulted to True?
        #     -> this also might no longer apply, pfm() wrapper
        #        (reload_filters_menu) could handle all this stuff without
        #        breaking a sweat, bothering uus() with bool/pfe seems
        #        unnecessary

        # flush filters array
        self.filters = []
        # flush the menu as well
        for a in self.filters_actions:
            self.menu_filters.removeAction(a)
        # flush the array of actions too
        self.filters_actions = []

        self.populate_filters_menu()

        # filter menu entries will be enabled if we don't...
        self.update_uielem_states()

        idx = None
        same_filter = False
        # seek by slug, check if argtype and key match, update index,
        # else, set last_filter none
        if self.last_filter is not None:
            for i in range(len(self.filters)):
                if self.filters[i].slug == self.last_filter.slug:
                    idx = i
                    break

            if idx is not None:
                # comparing last filter and probably last filter arg datatypes
                lf_args = self.last_filter.arg_specs
                plf_args = self.filters[idx].filter_args

                lf_argc = 0 if lf_args is None else len(lf_args)
                plf_argc = 0 if plf_args is None else len(plf_args)
                matching_args = 0

                if lf_argc == plf_argc:
                    # fetching data from NoneTypes won't make anyone happy
                    if lf_argc != 0:
                        # len is the same, so it doesn't matter which argc we
                        # take
                        for i in range(lf_argc):
                            # different key values can also break the deal
                            if lf_args[i].key_name == plf_args[i].key_name:
                                if lf_args[i].ui_widget == plf_args[i].ui_widget:
                                    matching_args += 1
                                # going from double to int and vice versa
                                # shouldn't be a big deal
                                # int can be truncated, double can add .0 to an
                                # int value
                                elif (lf_args[i].ui_widget == ArgUIWidget.QSpinBox and
                                     plf_args[i].ui_widget == ArgUIWidget.QDoubleSpinBox):
                                    matching_args += 1
                                elif (lf_args[i].ui_widget == ArgUIWidget.QDoubleSpinBox and
                                     plf_args[i].ui_widget == ArgUIWidget.QSpinBox):
                                    matching_args += 1

                    if lf_argc == matching_args:
                        same_filter = True

            if same_filter:
                self.last_filter.idx = idx
                self.last_filter.arg_specs = plf_args

                self.last_filter_params_reusable = True
            else:
                self.last_filter_params_reusable = False

            self.update_reapplyitem()


    def populate_filters_menu(self):
        # purge all __init__ things so that they don't end up in the fpathl and
        # parser log as a result
        self.clean_filters_folder()

        fpathl = glob.glob(self.abs_path('filters/*.py'))

        fcount = 0

        # we need this to handle imports between filters
        init = self.abs_path("filters/__init__.py")
        open(init, 'w').close()

        # TODO[FR]: sort successfully parsed fheads by FilterMeta.name, and ...
        # ... then do the importing
        # -> so, there should be a list of fheads plus the belonging paths

        for f in fpathl:
            fhead = parse_fhead(f)
            if fhead is not None:
                fcount += 1

                fs = os.path.split(f)[1]
                fs = fs.rsplit('.', 1)[0]
                mod_name = string.join(["filters-", fs], '')

                # at this part, we don't have to try to load_source(), it has
                # already been tried in parse_fhead()
                mod = imp.load_source(mod_name, f)
                funcs = inspect.getmembers(mod, inspect.isfunction)

                filter_fobj = None

                for ftuple in funcs:
                    if ftuple[0] == fhead.funcname:
                        filter_fobj = ftuple[1]
                        break
                    # no else, everything verified in parse_fhead()

                # new fmeta should contain fhead and the function object
                tmp = FilterEntry(filter_fobj, mod_name, fhead)
                self.filters.append(tmp)

                # TODO[FR]: find a unique hotkey for each menu entry to ...
                # ... prefix it with '&'
                if fhead.args is not None:
                    filter_name = string.join(['&', fhead.name, '...'], '')
                else:
                    filter_name = string.join(['&', fhead.name], '')
                filter_action = QtGui.QAction(filter_name, self)
                # don't call setStatusTip() if fhead.desc is empty
                if fhead.desc != '':
                    filter_action.setStatusTip(fhead.desc)
                # fcount counts how much valid filters have been found, to make
                # it suitable for filter index, a decrement is necessary
                fidx = fcount - 1
                filter_action.setObjectName(str(fidx))
                # this fella should inform filter_handler() about the clicked
                # entry index in filter menu
                filter_action.triggered.connect(self.filter_handler)

                self.filters_actions.append(filter_action)
                self.menu_filters.addAction(self.filters_actions[fidx])

        if fcount == 0:
            no_filters_action = QtGui.QAction("&No filters available", self)
            no_filters_action.setEnabled(False)

            self.filters_actions.append(no_filters_action)
            self.menu_filters.addAction(self.filters_actions[0])

        # purge all __init__ thingies again to make filter importing mechanism
        # at least a little bit less obvious
        self.clean_filters_folder()


    def abs_path(self, rel_path):
        return string.join([self.app_cwd, '/', rel_path], '')


    def clean_filters_folder(self):
        for f in glob.glob(self.abs_path('filters/__init__.*')):
            os.remove(f)


    def reload_reapply_last_filter(self):
        self.reload_filters_menu()

        if self.last_filter_params_reusable:
            self.reapply_last_filter()
        else:
            self.show_statusbar_msg("Filter no longer exists. Reinstall the missing filter or apply a new filter and try again.")

    def reapply_last_filter(self):
        # hoping the caller is smart enough, we shouldn't need to bother here a
        # lot
        self.run_filter(self.last_filter.idx, self.last_filter.arg_vals)


    def run_filter(self, idx, vals = None):
        # idx represents the filter index in self.filters
        # vals is a dict containing key-value pairs

        # what does it do? it:
        #  * gets the filter from the Filter array
        #  * spawns appropriate filter window, constructed according to arg
        #    specs
        #  * window executed, terminated, values collected in an arg dict
        #  * filter args stored to be applied again later
        #  * executes filter function with args we just got
        #  * on success replace cur_img and append undo history

        f = self.filters[idx]
        n = f.filter_name
        fo = f.function_obj
        a = f.filter_args

        if vals is None:
            if a is not None:
                vals = QSFilterDialog(n, a, self).run()
            else:
                vals = []
        # else if vals is not None, user didn't cancel the dialog, and there
        # probably is something in vals that could be fed to the the filter,
        # do nothing in this case then

        # TODO[FR]: progress bar!
        if vals is not None:
            # allowing broken plugins to re-run too, making filter development
            # by trial and error a bit easier,
            # could perhaps be a setting in the future (something like
            # "Store last filter data even if filter execution fails")
            # ... and in that case there should be a settings
            # registry/file/something
            self.last_filter = FilterParams(f.slug, a, vals, idx)
            self.last_filter_params_reusable = True

            # update the reapply menu item even if the filter fails
            self.update_reapplyitem()
            try:
                i = fo(self.cur_img, vals)
            except Exception as e:
                self.show_err_dlg(
                    string.join(["An error has occurred while running ", n, ". Please try adjusting the parameters or contact the filter author."], ''),
                    str(e)
                )
            else:
                # let's check if the filter returned a proper ndarray
                if not isinstance(i, ndarray):
                    self.show_err_dlg(string.join([n, " did not return a valid image object. Please try adjusting the parameters or contact the filter author."], ''))
                else:
                    # only the filters that finish successfully are stored in
                    # the image_history as it would otherwise be a pure waste
                    # of memory. we'd have to store the unchanged cur_img as
                    # the filter failed to return something meaningful or it
                    # returned non-image data, which is not really useful to
                    # any party (the end user or the filter developer)

                    # if user modifies a version of an image which isn't the
                    # last one, discard every version after it
                    if self.image_history_idx != len(self.image_history) - 1:
                        self.image_history = self.image_history[:self.image_history_idx + 1]

                    self.image_history.append(ImageHistoryEntry(n, i, False))
                    self.image_history_idx = len(self.image_history) - 1
                    self.cur_img = i

                    self.update_window()


    def filter_handler(self):
        # filter menu index should be passed by the caller (er, sender) as the
        # objectName
        # sender's objectName should match the self.filters index
        idx = int(self.sender().objectName())
        self.run_filter(idx)

    def update_window(self):
        self.update_title()
        self.update_undoitems()
        # TODO: update_imgpreview() should be a signal+slot, probably.
        self.update_imgpreview()

    def edit_undo(self):
        self.image_history_idx -= 1
        self.ihi_changed()

    def edit_redo(self):
        self.image_history_idx += 1
        self.ihi_changed()

    def ihi_changed(self):
        # TODO: ihi_changed() is probably a signal too.
        self.cur_img = self.image_history[self.image_history_idx].image
        self.update_window()

    def update_undoitems(self):
        if self.image_history_idx == 0:
            undotext = "Undo"
        else:
            undotext = string.join(
                ["Undo", self.image_history[self.image_history_idx].name],
                ' '
            )

        # has the redo button hit the end?
        if self.image_history_idx == len(self.image_history) - 1:
            redotext = "Redo"
        # is there anything in front of it?
        elif self.image_history_idx < len(self.image_history) - 1:
            redotext = string.join(
                ["Redo", self.image_history[self.image_history_idx + 1].name],
                ' '
            )
        # then it must be at the beginning (ihi = 0) and nothing to redo.
        else:
            redotext = "Redo"

        self.edit_undo_action.setText(undotext)
        self.edit_redo_action.setText(redotext)

    def view_zoomout(self):
        # reset zoom when opening new files
        # don't touch zoom when altering current files (applying filters)
        self.zoom_idx += 1
        self.update_imgpreview()

    def view_zoomin(self):
        self.zoom_idx -= 1
        self.update_imgpreview()

    def view_zoomlvlchanged(self):
        self.zoom_idx = self.tool_bar_zoomdropdown.currentIndex()
        self.update_imgpreview()

    def update_reapplyitem(self):
        if self.last_filter_params_reusable:
            filter_name = self.filters[self.last_filter.idx].filter_name
            self.filters_reapplylastfilter_action.setText(string.join(["&Reapply", filter_name], ' '))
            self.filters_reapplylastfilter_action.setEnabled(True)
        else:
            self.filters_reapplylastfilter_action.setText("&Reapply last filter")
            self.filters_reapplylastfilter_action.setEnabled(False)


    def update_uielem_states(self):
        img_opened = False if self.cur_img is None else True
        
        min_zoom_reached = self.zoom_idx == len(self.zoom_levels) - 1
        max_zoom_reached = self.zoom_idx == 0

        min_ihi_reached = self.image_history_idx == 0
        max_ihi_reached = self.image_history_idx == len(self.image_history) - 1

        self.file_save_action.setEnabled(img_opened)
        self.file_saveas_action.setEnabled(img_opened)

        self.view_zoomout_action.setEnabled(
            img_opened and not min_zoom_reached
        )
        self.view_zoomin_action.setEnabled(
            img_opened and not max_zoom_reached
        )

        self.tool_bar_zoomdropdown.setEnabled(img_opened)

        self.edit_undo_action.setEnabled(
            img_opened and not min_ihi_reached
        )
        self.edit_redo_action.setEnabled(
            img_opened and not max_ihi_reached
        )

        if len(self.filters) != 0:
            for a in self.filters_actions:
                a.setEnabled(img_opened)

        last_filter_exists = self.last_filter is not None
        self.tool_bar_reloadreapplyfilter.setEnabled(last_filter_exists)

    def update_title(self):
        # TODO[RS]: update_title could be a signal handler
        rel_file_name = self.cur_img_path.rsplit('/', 1)
        # TODO[RS]: do we really need this thing below?
        rel_file_name = rel_file_name[0] if len(rel_file_name) == 1 else rel_file_name[1]
        basetitle = " - SIMPL"
        if self.is_cur_img_saved():
            title = string.join(['[', rel_file_name, ']', basetitle], '')
        else:
            title = string.join(['*', ' [', rel_file_name, ']', basetitle], '')
        self.setWindowTitle(title)


    # override the default QMainWindow closeEvent method to handle unsaved
    # changes
    def closeEvent(self, event):
        if self.handle_file_leave():
            QtGui.QMainWindow.closeEvent(self, event)
        else:
            event.ignore()


def main():
    # this enables PyCharm to send a SIGINT instead of SIGTERMing
    # also makes CTRL-C work
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    app = QtGui.QApplication(sys.argv)

    # TODO[FR]: check if path is a valid path (WIP)
    args = app.arguments()
    # drop the application path as it doesn't really help us much here
    args = None if len(args) == 1 else args[1:]

    path = None
    no_err_dlgs = False

    if args is not None:
        for a in args:
            if a[0] == '-':
                # ooh, a flag?
                # "-s" stands for silent
                if a == "-s" or a == "--no-error-dialogs":
                    no_err_dlgs = True
                else:
                    print("Bad option: " + a)
                    print_usage_and_quit()
            elif os.path.isfile(a):
                if path is None:
                    path = a
                else:
                    print("Image path already defined.")
                    print_usage_and_quit()
            else:
                print("Invalid argument provided.")
                print_usage_and_quit()

    main_window = QSMain(path, no_err_dlgs)

    sys.exit(app.exec_())

def print_usage_and_quit():
    print("\n" + \
          "USAGE:" + "\n" + \
          "  main.py [args] [path]" + "\n" + \
          "ARGUMENTS:" + "\n" + \
          "  -s, --no-error-dialogs \t Silent operation, logs all errors to the console instead of showing error dialogs")
    sys.exit(1)


if __name__ == "__main__":
    main()
