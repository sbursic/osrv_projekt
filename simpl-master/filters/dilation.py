## begin fhead
#
# funcname "sb_dilation"
# name "Dilation"
# desc "The task of dilation is to increase the boundaries of the foreground pixels "
# arg "kernel_size" spinbox int [3, 30] "Kernel size"
#
## end fhead

import numpy as np

def sb_dilation(image,args):
    image_w=image.shape[0]
    image_h=image.shape[1]

    kernel_size = args['kernel_size']

    kernel = np.ones((kernel_size,kernel_size),np.uint8)
    
    kernel_w=kernel.shape[0]
    kernel_h=kernel.shape[1]
    
    h=kernel_h//2    
    w=kernel_w//2
    #print ("W :")
    #print(w)
    #print ("H :")
    #print(h)
    
    dilation_image= image.copy()
    
    #dilation_image = image
    
    #for i in range(h, image_h-h):
        #for j in range(w, image_w-w):
    for i in range(0, image_w):
        for j in range(0, image_h):  ## Prolazi kroz svaki piksel
            
            if((image[i][j]== 0).any()):
                #print ("Background: ")
                #print ("i je "+str(i))
                #print("j je "+ str(j)+"\n")
                #counter = False
           
                for n in range(0, kernel_w):
                    for m in range(0, kernel_h):
                        if ( ((i-w+n)>=0) and ((j-h+m)>=0) and ((i-w+n)<=image_w-1) and ((j-h+m)<=image_h-1)  ):
                            #print (" kernel: ")
                            #print ("i je "+str(i-w+n))
                            #print("j je "+ str(j-h+m)+"\n")
                            
                            if ((image[i-w+n][j-h+m] == 255).any()):
                                dilation_image[i-w+w][j-h+h] =255
                                #counter=True
                       
                            
                            
            #if (counter==True):
                #dilation_image[i][j]=255
           # else :
                #print("foreground: ")
                #print ("i je "+str(i))
                #print("j je "+ str(j)+"\n")
                   
    return dilation_image