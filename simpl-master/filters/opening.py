## begin fhead
#
# funcname "sb_opening"
# name "Opening"
# desc "Just like erosion, removes some of the foreground pixels, but is not as destructive. "
# arg "kernel_size" spinbox int [3, 30] "Kernel size"
#
## end fhead

import numpy as np


def sb_opening(image,args):
    image_w=image.shape[0]
    image_h=image.shape[1]

    kernel_size = args['kernel_size']
    kernel = np.ones((kernel_size,kernel_size),np.uint8)

    kernel_w=kernel.shape[0]
    kernel_h=kernel.shape[1]
    
    h=kernel_h//2
    w=kernel_w//2
    #image= sbErosion(image,kernel)

    erosion_image= image.copy()
    
    for i in range(0, image_w):
        for j in range(0, image_h):
            if((image[i][j]== 255).any()):
                
                for n in range(0, kernel_w):
                    for m in range(0, kernel_h):
                        if ( ((i-w+n)>=0) and ((j-h+m)>=0) and ((i-w+n)<=image_w-1) and ((j-h+m)<=image_h-1)  ):

                            if ((image[i-w+n][j-h+m]== 0).any()):
                                erosion_image[i-w+w][j-h+h] =0
                            
                            ## trazimo svaki foreground i ako je bar jedan od piksel ispod kernella background
                            ## onda i on postaje background
               
    image = erosion_image
    
    #image= sbDilation(image,kernel)

    dilation_image= image.copy()
    
    #dilation_image = image
    
    #for i in range(h, image_h-h):
        #for j in range(w, image_w-w):
    for i in range(0, image_w):
        for j in range(0, image_h):  ## Prolazi kroz svaki piksel
            
            if((image[i][j]== 0).any()):
                
                for n in range(0, kernel_w):
                    for m in range(0, kernel_h):
                        if ( ((i-w+n)>=0) and ((j-h+m)>=0) and ((i-w+n)<=image_w-1) and ((j-h+m)<=image_h-1)  ):
                            #print (" kernel: ")
                            #print ("i je "+str(i-w+n))
                            #print("j je "+ str(j-h+m)+"\n")
                            
                            if ((image[i-w+n][j-h+m] == 255).any()):
                                dilation_image[i-w+w][j-h+h] =255
                                #counter=True
                       
                   
    return dilation_image



    