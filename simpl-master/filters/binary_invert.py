## begin fhead
#
# funcname "sb_invert"
# name "Binary Invert"
# desc "Invert black and white of a binary image "
#
## end fhead



def sb_invert(image,args):
    image_w=image.shape[0]
    image_h=image.shape[1]

    inverted_image= image.copy()

    for i in range(0, image_w):
        for j in range(0, image_h):

            if ((image[i][j]==0).any()):
                inverted_image[i][j]=255
            if ((image[i][j]==255).any()):
                inverted_image[i][j]=0

    return inverted_image        