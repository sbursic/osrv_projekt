## begin fhead
#
# funcname "sb_threshold"
# name "Threshold"
# desc "Makes a binary image of a greyscale image, pixels lower then threshold number turn white, others turn black "
# arg "thresh" spinbox int [-1, 255] "Threshold Number"
#
## end fhead



def sb_threshold(img,args):  
    
    thresh= args['thresh']
    

    img_w=img.shape[0]
    img_h=img.shape[1]

    binary_img= img.copy()

    for i in range(0, img_w):
        for j in range(0, img_h):

            if ((binary_img[i][j] >= 0).any() and (binary_img[i][j] <= thresh).any()):
                    binary_img[i][j]= 0

            if ((binary_img[i][j] >= thresh+1).any() and (binary_img[i][j] <= 255).any()):
                    binary_img[i][j]= 255
    img= binary_img
    return img
            